compile = qmk compile
flash = qmk flash

all: custom cheapino x7 bm40 cstc

custom:
	$(compile) -kb custom36

cheapino:
	$(compile) -kb cheapino

x7:
	$(compile) -kb xtips/x7s

bm40:
	$(compile) -kb kprepublic/bm40hsrgb

cstc:
	$(compile) -kb kprepublic/cstc40/single_pcb

setup:
	echo "setup QMK"
	qmk setup -H ${PWD}/qmk_firmware
	qmk config user.keymap=custom
	qmk config user.keyboard=custom36

	echo "Add cheapino branch"
	cd qmk_firmware &&\
		git remote add tompi https://github.com/tompi/qmk_firmware &&\
		git fetch tompi cheapinov2 &&\
		git checkout tompi/cheapinov2

	echo "Link folders of keyboards"
	ln -s ${PWD}/custom36 ${PWD}/qmk_firmware/keyboards/custom36
	ln -s ${PWD}/bm40hsrgb ${PWD}/qmk_firmware/keyboards/kprepublic/bm40hsrgb/rev1/keymaps/custom
	ln -s ${PWD}/cstc40 ${PWD}/qmk_firmware/keyboards/kprepublic/cstc40/keymaps/custom

	mkdir ${PWD}/qmk_firmware/keyboards/xtips
	ln -s ${PWD}/x7s ${PWD}/qmk_firmware/keyboards/xtips/x7s
