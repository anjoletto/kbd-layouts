#include QMK_KEYBOARD_H

enum layers {
    _LETTERS,
    _CONTROL,
    _SYMBOLS,
    _STUFF
};

/******************************************************************************/

#define L_SYMBS MO(_SYMBOLS)
#define L_STUFF MO(_STUFF)
#define FL_ZERO LT(_STUFF, KC_0)
#define CL_SPC  LT(_CONTROL, KC_SPC)

#define LCT_0   MT(MOD_LCTL, KC_0)
#define RAT_DOT MT(MOD_RALT, KC_DOT)
#define LAT_COM MT(MOD_LALT, KC_COMM)

/******************************************************************************/

const key_override_t circ_override = ko_make_basic(MOD_MASK_ALT, KC_CIRC, RALT(KC_6));
const key_override_t colon_override = ko_make_basic(MOD_MASK_SHIFT, KC_COLN, KC_SCLN);
const key_override_t bspc_override = ko_make_basic(MOD_MASK_SHIFT, KC_BSPC, KC_DEL);

const key_override_t *key_overrides[] = {
    &circ_override,
    &colon_override,
    &bspc_override
};

/******************************************************************************/

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_LETTERS] = LAYOUT_split_3x5_3(
   //|--------+--------+--------+--------+--------| |--------+--------+--------+--------+--------|
      KC_Q,    KC_W,    KC_F,    KC_P,    KC_G,      KC_J,    KC_L,    KC_U,    KC_Y,    KC_COLN,
      KC_A,    KC_R,    KC_S,    KC_T,    KC_D,      KC_H,    KC_N,    KC_E,    KC_I,    KC_O,
      KC_LCTL, KC_Z,    KC_X,    KC_C,    KC_V,      KC_B,    KC_K,    KC_M,    LAT_COM, RAT_DOT,
   //|--------+--------+--------+--------+--------| |--------+--------+--------+--------+--------|
                        XXXXXXX, L_SYMBS, KC_LSFT,   XXXXXXX, CL_SPC,  XXXXXXX
   //                  |--------+--------+--------| |--------+--------+--------|
    ),
    [_CONTROL] = LAYOUT_split_3x5_3(
   //|--------+--------+--------+--------+--------| |--------+--------+--------+--------+--------|
      KC_TAB,  KC_7,    KC_8,    KC_9,    XXXXXXX,   KC_HOME, KC_PGDN, KC_PGUP, KC_END,  KC_BSPC,
      KC_ESC,  KC_4,    KC_5,    KC_6,    KC_APP,    KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT, KC_ENT,
      LCT_0,   KC_1,    KC_2,    KC_3,    KC_CALC,   KC_MPRV, KC_MPLY, KC_MNXT, KC_PSCR, KC_LSFT,
   //|--------+--------+--------+--------+--------| |--------+--------+--------+--------+--------|
                        _______, FL_ZERO, KC_LGUI,    _______, _______, _______
   //                  |--------+--------+--------| |--------+--------+--------|
    ),
    [_SYMBOLS] = LAYOUT_split_3x5_3(
   //|--------+--------+--------+--------+--------| |--------+--------+--------+--------+--------|
      KC_TILD, KC_AT,   KC_HASH, KC_DLR,  KC_PERC,   KC_CIRC, KC_AMPR, KC_ASTR, KC_MINS, KC_DEL,
      KC_BSLS, KC_LCBR, KC_LBRC, KC_LPRN, KC_LT,     KC_GT,   KC_RPRN, KC_RBRC, KC_RCBR, KC_SLSH,
      KC_EXLM, KC_PIPE, KC_UNDS, KC_QUOT, KC_GRV,    KC_SCLN, KC_DQUO, KC_EQL,  KC_PLUS, KC_QUES,
   //|--------+--------+--------+--------+--------| |--------+--------+--------+--------+--------|
                        _______, _______, _______,   _______, L_STUFF, _______
   //                  |--------+--------+--------| |--------+--------+--------|
    ),
    [_STUFF] = LAYOUT_split_3x5_3(
   //|--------+--------+--------+--------+--------| |--------+--------+--------+--------+--------|
      KC_F1,   KC_F2,   KC_F3,   KC_F4,   XXXXXXX,   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
      KC_F5,   KC_F6,   KC_F7,   KC_F8,   XXXXXXX,   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
      KC_F9,   KC_F10,  KC_F11,  KC_F12,  XXXXXXX,   XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX, XXXXXXX,
   //|--------+--------+--------+--------+--------| |--------+--------+--------+--------+--------|
                        _______, _______, _______,   _______, _______, _______
   //                  |--------+--------+--------| |--------+--------+--------|
    )
};

/******************************************************************************/
